package com.androsor99.dao;

import com.androsor99.entity.ConfigFile;
import com.androsor99.exception.DaoException;
import com.androsor99.util.PropertiesUtil;
import com.androsor99.validator.SchemaValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConfigFileFromXmlDao implements Dao<ConfigFile> {

    public final static String CONFIG_FILE_KEY = "config_file";
    public final static String SCHEMA_CONFIG_FILE_KEY = "schema_config_file";
    private static final ConfigFileFromXmlDao INSTANCE = new ConfigFileFromXmlDao();
    private static final SchemaValidator schemaValidator = SchemaValidator.getInstance();
    private static final Logger logger = LogManager.getLogger(ConfigFileFromXmlDao.class);

    @Override
    public ConfigFile create() throws DaoException {
        String filePath = PropertiesUtil.get(CONFIG_FILE_KEY);
        String pathSchema = PropertiesUtil.get(SCHEMA_CONFIG_FILE_KEY);
        String fileName = getFileName(filePath);
        String schemaName = getFileName(pathSchema);
        try {
           schemaValidator.validateXMLSchema(pathSchema, filePath);
           return mapConfigFile(filePath, fileName);
        } catch (IOException e) {
            logger.fatal("The \"{}\" or \"{}\" was not read", fileName, schemaName, e);
            throw new DaoException(e);
        } catch (SAXException e) {
           logger.error("The file \"{}\" does not match XSD-schema \"{}\"", fileName, schemaName);
           throw new DaoException(e);
        }
    }

    private ConfigFile mapConfigFile(String filePath, String fileName) throws IOException {
        try (FileInputStream fileInputStream = new FileInputStream(filePath)) {
            ObjectMapper mapper = new XmlMapper();
            ConfigFile configFile = mapper.readValue(fileInputStream, ConfigFile.class);
            logger.info("The \"{}\" was successfully read", fileName);
            return configFile;
        }
    }

    private String getFileName(String path) {
        return Path.of(path).getFileName().toString();
    }
    
    public static ConfigFileFromXmlDao getInstance() {
        return INSTANCE;
    }
}
