package com.androsor99.controller;

import com.androsor99.exception.ServiceException;

public interface Controller {

    void doAction() throws ServiceException;
}
