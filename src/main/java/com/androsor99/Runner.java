package com.androsor99;

import com.androsor99.controller.RenamingBySuffixController;
import com.androsor99.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Runner {

    private static final RenamingBySuffixController controller = RenamingBySuffixController.getInstance();
    private static final Logger logger = LogManager.getLogger(Runner.class);

    public static void main(String[] args) {

        logger.info("______________________Entering application______________________");
        try {
            controller.doAction();
        } catch (ServiceException e) {
            logger.error("Application terminated with an error", e);
        }
        logger.info("Exiting application.");
    }
}
