package com.androsor99.service;

import com.androsor99.dao.ConfigFileFromXmlDao;
import com.androsor99.entity.ConfigFile;
import com.androsor99.exception.DaoException;
import com.androsor99.exception.ServiceException;
import com.androsor99.service.dto.OutputResult;
import com.androsor99.validator.FileValidator;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RenamingBySuffixService implements Service<OutputResult> {

    private static final RenamingBySuffixService INSTANCE = new RenamingBySuffixService();
    private static final Logger logger = LogManager.getLogger(RenamingBySuffixService.class);
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    private final ConfigFileFromXmlDao file = ConfigFileFromXmlDao.getInstance();
    private final FileValidator fileValidator = FileValidator.getInstance();

    public OutputResult rename() throws ServiceException {
        try {
            ConfigFile configFile = file.create();
            OutputResult outputResult = new OutputResult();
            outputResult.setDateAndTime(ZonedDateTime.now().format(DATE_TIME_FORMATTER));
            outputResult.setFiles(renameFiles(configFile));
            return outputResult;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    private List<OutputResult.File> renameFiles(ConfigFile configFile) {
        String suffix = configFile.getSuffix();
        List<Path> filePaths = configFile.getFilePaths();
        List<OutputResult.File> fileNames = new ArrayList<>();
        String oldFileName;
        String newFileName;
        for (Path path : filePaths) {
            oldFileName = getNameFile(path);
            newFileName = addSuffixToName(oldFileName, suffix);
            Path filePathWithNewName = Paths.get(path.getParent().toString(), newFileName);
            if (fileValidator.isFileExist(filePathWithNewName)) {
                logger.error("The file \"{}\" is not renamed. Invalid new name \"{}\". A file with the same name already exists.", oldFileName, newFileName);
            } else if (fileValidator.isFileExist(path)) {
                fileNames.addAll(getOldAndNewFileName(path, oldFileName, newFileName));
            } else {
                logger.error("The file {} does not exist", oldFileName);
            }
        }
        return fileNames;
    }

    private List<OutputResult.File> getOldAndNewFileName(Path filePath, String oldFileName, String newFileName) {
        List<OutputResult.File> fileNames = new ArrayList<>();
        try {
            move(filePath, newFileName);
            fileNames.add(new OutputResult.File(oldFileName, newFileName));
            logger.info("File \"{}\" renameMessage successfully. New file {}.", oldFileName, newFileName);
        } catch (IOException e) {
            logger.error("The file {} has not been renamed", oldFileName, new ServiceException(e));
        }
        return fileNames;
    }

    private String getNameFile(Path path) {
        return path.getFileName().toString();
    }

    private String addSuffixToName(String name, String suffix) {
        return String.join(suffix, name.substring(0, name.lastIndexOf(".")), name.substring(name.lastIndexOf(".")));
    }

    private void move(Path path, String fileName) throws IOException {
        Files.move(path, path.resolveSibling(fileName), REPLACE_EXISTING);
    }

    public static RenamingBySuffixService getInstance() {
        return INSTANCE;
    }
}
