package com.androsor99.service.dto;

import com.androsor99.dao.ConfigFileFromXmlDao;
import com.androsor99.util.PropertiesUtil;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OutputResult {

    private final String fileConfig = PropertiesUtil.get(ConfigFileFromXmlDao.CONFIG_FILE_KEY);
    private String dateAndTime;
    @JacksonXmlElementWrapper(localName = "files")
    private List<File> files;
    @JsonGetter("file")
    public List<File> getFiles() {
        return files;
    }

    @Data
    @AllArgsConstructor
    public static class  File {
        private String oldName;
        private String newName;
    }
}
