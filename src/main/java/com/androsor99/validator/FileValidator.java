package com.androsor99.validator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import java.nio.file.Files;
import java.nio.file.Path;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FileValidator {

    private static final FileValidator INSTANCE = new FileValidator();

    public boolean isFileExist(Path path) {
        return Files.exists(path);
    }

    public static FileValidator getInstance() {
        return INSTANCE;
    }
}
