package com.androsor99.view;

import com.androsor99.service.dto.OutputResult;
import com.androsor99.util.PropertiesUtil;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PrinterToFileXML implements Printer<OutputResult> {

    private static final PrinterToFileXML INSTANCE = new PrinterToFileXML();
    private static final Logger logger = LogManager.getLogger(PrinterToFileXML.class);
    private static final String FILE_XML_KEY = "output_xml";

    @Override
    public void print(OutputResult result) {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
        try {
            xmlMapper.writerWithDefaultPrettyPrinter().writeValue(new File(PropertiesUtil.get(FILE_XML_KEY)) , result);
            logger.info("Document XML save to file.");
        } catch (IOException e) {
            logger.error("Document XML doesn't save to file.", e);
        }
    }

    public static PrinterToFileXML getInstance() {
        return INSTANCE;
    }
}
