## 6. XML and JSON in Java

###Home task [SuffixingApp Structured Logging Implementation]

### _Description_

Get Suffixing App project from Logging topic. Make its output structured.

### _App Specification_

It is a Suffixing App - a small java application that refers to a config file and renames a set of files and renames them adding a suffix specified in the same config.

_Changes:_ config file now should be an XML file.

### _Details:_

*   Application should read a config file on the startup.
*   Then it should ensure that all files from the config exist.
*   Then it should rename each file adding a suffix from the config to its name.

### _Logging Specification_

### _Logging spec of previous exercise:_

*   Application should log startup information.
*   Application should log information on config read.
*   Application should log renaming process information.
*   Application should log summary information.
*   Application should log shutdown information.
*   Application should handle and log possible errors.

Use different logging level. All log entries should contain a date and time information as well.

### _Changes:_

* When renaming is finished the application should print a document of completed actions.
  Document should be XML-based. It should contain:
    *   config file name
    *   execution time
    *   list of files with old and new names
*   All the logging entries from previous exercise should become JSON document of some structure. They should contain:
    *   date and time
    *   message
    *   severity label
    *   error info, if its error

### _Steps_

1. Complete the project to meet specifications from previous exercise.
2. Show the mentor your results.